/*  libpic - collection of image display and rendering routines

    Stephen Smith, Christian Beckmann and Matthew Webster, FMRIB Image Analysis Group

    Copyright (C) 1999-2009 University of Oxford  */

/*  CCOPYRIGHT */


#ifndef __MISCPIC_h
#define __MISCPIC_h

#include <stdarg.h>
#include "gd.h"

#include "newimage/newimageall.h"

namespace MISCPIC{

  //template <class T>
  class miscpic
  {
  public:

    //constructor
    miscpic(){
      nlut = 0;
      compare= 0;
      writeText=false;
      LR_label_flag = true;
      markRight=false;
      trans= -10;
      edgethresh = 0.0;
      if(getenv("FSLDIR")!=0){
        lutbase = std::string(getenv("FSLDIR")) + "/etc/luts/";
      }
      else{
        lutbase = std::string("/");
      }
      title = std::string("");
      cbartype = std::string("");
      cbarptr = NULL;
      outim = NULL;
      picr = NULL;
      picg = NULL;
      picb = NULL;
    };

    ~miscpic(){
      if(picr!=NULL) free(picr);
      if(picg!=NULL) free(picg);
      if(picb!=NULL) free(picb);
      if(cbarptr) gdImageDestroy(cbarptr);
      if(outim) gdImageDestroy(outim);
    }

    int slicer(const NEWIMAGE::volume<float>& vol1,const NEWIMAGE::volume<float>& vol2,const char *opts, bool labelSlices=false, bool debug = false);
    int slicer(const NEWIMAGE::volume<float>& vol1,const NEWIMAGE::volume<float>& vol2,std::vector<std::string> inputOptions, bool labelSlices=false, bool debug = false);

    inline int slicer(const NEWIMAGE::volume<float>& vol1,const char *opts, bool labelSlices=false, bool debug = false)
    { NEWIMAGE::volume<float> tmp(1,1,1);
      return this->slicer(vol1, tmp, opts, debug);}

    int write_png ( char *filename, int x_size, int y_size,
                    unsigned char *r, unsigned char *g, unsigned char *b);
    int write_pgm ( char *filename, int x_size, int y_size,
                    unsigned char *i );
    int write_ppm ( char *filename, int x_size, int y_size,
                    unsigned char *r, unsigned char *g, unsigned char *b );
    void write_pic(char *fname, int width, int height);

    inline void set_title(std::string what) { title = what;}
    inline void set_cbar(std::string what) { cbartype = what;}

    inline void read_lut(char *fname){
      lut = std::string(fname); this->read_lut();
    }

    void read_lut();

    inline void set_minmax(float bgmin, float bgmax, float s1min,
                           float s1max){
      this->set_minmax(bgmin, bgmax, s1min, s1max, (float)0.0, float(0.0));
    };

    void set_minmax(float bgmin, float bgmax, float s1min,
                    float s1max, float s2min, float s2max);

    inline int overlay(NEWIMAGE::volume<float>& newvol, NEWIMAGE::volume<float>& bg, NEWIMAGE::volume<float>& s1,
                       NEWIMAGE::volume<float>& s2, float bgmin, float bgmax, float s1min,
                       float s1max, float s2min, float s2max, int colour_type,
                       int checker,
                       bool out_int = false, bool debug = false){
      return this->overlay(newvol, bg, s1, s2, bgmin, bgmax, s1min,
                           s1max, s2min, s2max,
                           colour_type, checker,
                           std::string(""), std::string(""), out_int, debug);
    }

    int overlay(NEWIMAGE::volume<float>& newvol, NEWIMAGE::volume<float>& bg, NEWIMAGE::volume<float>& s1,
                NEWIMAGE::volume<float>& s2, float bgmin, float bgmax, float s1min,
                float s1max, float s2min, float s2max, int colour_type,
                int checker,
                std::string cbarfname, std::string cbartype, bool out_int = false,
                bool debug = false);

  private:

    int x_size, y_size, z_size, size, x_size_pic, y_size_pic,
	  z_size_pic, nlut, compare, trans;

    bool debug, LR_label_flag, writeText;

    float edgethresh;

    std::string lut, lutbase, title, cbartype;

    gdImagePtr cbarptr, outim;

    bool markRight;

    std::vector<int> rlut, glut, blut; //stores the lut;
    unsigned char *picr, *picg, *picb;

    //volume<T>  inp1, inp2, imr, img, imb;
    NEWIMAGE::volume<float>  inp1, inp2, imr, img, imb;
    std::vector<float> minmax; //will store min and max values for bg and stats images
    //needed for colorbar

    void sag(float xx, int p, int width);
    void cor(float yy, int p, int width);
    void axi(float zz, int p, int width);

    int create_cbar(std::string cbartype);
    int write_cbar(std::string fname, std::string cbartype);
    int add_cbar(std::string cbartype);

    int add_title(int width);
    void addRlabel(int p, int width, int size_pic, int alt_size_pic,
                   bool onleft);
    void addRlabel(unsigned char* picr, int p, int width, int size_pic,
                   int alt_size_pic, bool onleft);
  };
}

#endif
