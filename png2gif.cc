/*    Copyright (C) 2012 University of Oxford  */

/*  CCOPYRIGHT  */

#include <stdio.h>
#include "miscplot.h"

int main()
{
  gdImagePtr im;

  FILE  *inpng  = fopen("hello.png", "rb" );
  FILE  *outgif = fopen("hello.gif", "wb" );
  im = gdImageCreateFromPng(inpng);
  gdImageGif(im, outgif);
}
