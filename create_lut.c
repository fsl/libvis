/* {{{ Copyright etc. */

/*  create_lut - create MEDx LUTs and material maps for colour rendering

    Stephen Smith, FMRIB Image Analysis Group

    Copyright (C) 1999-2001 University of Oxford  */

/*  CCOPYRIGHT */

/* }}} */
/* {{{ includes and defines */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

/* }}} */
/* {{{ main() */

#define NLUTS 3

/* note that for number 2 (the third map) only the render3.lut makes sense */

int main(argc,argv)
  int argc;
  char *argv[];
{
  /* {{{ variables */

FILE   *lut[NLUTS], *map[NLUTS], *lutt[NLUTS], *mapt[NLUTS];
int    i1=0, i2=0, j, k, l;
double a, intensity=0, red, green, blue;
char   filename[1000];

/* }}} */

  /* {{{ usage */

  if (argc<2)
  {
    printf("Usage: create_lut <output_file_root>\n");
    exit(1);
  }

/* }}} */
  /* {{{ prepare output files and print starting texts */

for(l=0;l<NLUTS;l++)
{
  sprintf(filename,"%s%d.lut",argv[1],l+1);  lut[l]=fopen(filename,"wb");
  sprintf(filename,"%s%d.map",argv[1],l+1);  map[l]=fopen(filename,"wb");

  sprintf(filename,"%s%dt.lut",argv[1],l+1); lutt[l]=fopen(filename,"wb");
  sprintf(filename,"%s%dt.map",argv[1],l+1); mapt[l]=fopen(filename,"wb");

  fprintf(lut[l],"%%!VEST-LUT\n%%%%BeginInstance\n<<\n/SavedInstanceClassName /ClassLUT \n/PseudoColorMinimum 0.00 \n/PseudoColorMaximum 1.00 \n/PseudoColorMinControl /Low \n/PseudoColorMaxControl /High \n/PseudoColormap [\n");
  fprintf(map[l],"%%!VEST-MaterialMap\n%%%%BeginInstance\n<<\n/SavedInstanceClassName /ClassMaterialMap \n/Name (Render Map) \n/Colors [\n");
  fprintf(lutt[l],"%%!VEST-LUT\n%%%%BeginInstance\n<<\n/SavedInstanceClassName /ClassLUT \n/PseudoColorMinimum 0.00 \n/PseudoColorMaximum 1.00 \n/PseudoColorMinControl /Low \n/PseudoColorMaxControl /High \n/PseudoColormap [\n");
  fprintf(mapt[l],"%%!VEST-MaterialMap\n%%%%BeginInstance\n<<\n/SavedInstanceClassName /ClassMaterialMap \n/Name (Render Map) \n/Colors [\n");
}

/* }}} */
  /* {{{ colour maps for luts and mmaps */

  /* {{{ type 0 */

for (a=0; a<1; a+=0.01)
{
  fprintf(lut[0],"<-color{%f,%f,%f}->\n",a,a,a);
  fprintf(map[0],"<-color{%f,%f,%f}->\n",a,a,a);
  i1++;
}
for (a=0; a<1; a+=0.01)
{
  fprintf(lut[0],"<-color{1.0,%f,0.0}->\n",a);
  fprintf(map[0],"<-color{1.0,%f,0.0}->\n",a);
  i1++;
}

/* }}} */
  /* {{{ type 1 */

for (a=0; a<1; a+=0.01)
{
  fprintf(lut[1],"<-color{%f,%f,%f}->\n",a,a,a);
  fprintf(map[1],"<-color{%f,%f,%f}->\n",a,a,a);
  i2++;
}
for (a=0; a<1; a+=0.01)
{
  fprintf(lut[1],"<-color{1.0,%f,0.0}->\n",a);
  fprintf(map[1],"<-color{1.0,%f,0.0}->\n",a);
  i2++;
}
for (a=0; a<1; a+=0.01)
{
  fprintf(lut[1],"<-color{0.0,%f,1.0}->\n",a);
  fprintf(map[1],"<-color{0.0,%f,1.0}->\n",a);
  i2++;
}
for (a=0; a<1; a+=0.005)
{
  fprintf(lut[1],"<-color{0.0,%f,0.0}->\n",a/2+0.5);
  fprintf(map[1],"<-color{0.0,%f,0.0}->\n",a/2+0.5);
  i2++;
}

/* }}} */
  /* {{{ type 2 */

for (a=0; a<1; a+=0.01) /* LB-B */
{
  fprintf(lut[2],"<-color{0.0,%f,1.0}->\n",1-a);
  fprintf(map[2],"<-color{0.0,%f,1.0}->\n",1-a);
  i2++;
}
for (a=0; a<1; a+=0.02) /* B-G */
{
  fprintf(lut[2],"<-color{%f,%f,%f}->\n",a/2,a/2,1-a/2);
  fprintf(map[2],"<-color{%f,%f,%f}->\n",a/2,a/2,1-a/2);
  i2++;
}
for (a=0; a<1; a+=0.02) /* G-R */
{
  fprintf(lut[2],"<-color{%f,%f,%f}->\n",(1+a)/2,(1-a)/2,(1-a)/2);
  fprintf(map[2],"<-color{%f,%f,%f}->\n",(1+a)/2,(1-a)/2,(1-a)/2);
  i2++;
}
for (a=0; a<1; a+=0.01) /* R-Y */
{
  fprintf(lut[2],"<-color{1.0,%f,0.0}->\n",a);
  fprintf(map[2],"<-color{1.0,%f,0.0}->\n",a);
  i2++;
}

/* }}} */

#define STARTINT 0.25
#define K1 5
#define K2 10
#define K3 20

  /* {{{ type t0 */

for (a=0; a<1; a+=0.01)
{
  fprintf(lutt[0],"<-color{%f,%f,%f}->\n",a,a,a);
  fprintf(mapt[0],"<-color{%f,%f,%f}->\n",a,a,a);
}

for(j=0; j<5; j++)
  for(k=0; k<K3; k++)
  {
    if (k<K1)              intensity = STARTINT;
    if ((k>=K1) && (k<K2)) intensity = ((double)k-K1)/((K2-K1-1)/(1-STARTINT)) + STARTINT;
    if (k>=K2)             intensity = 1;

    red=1.0 * intensity;
    green=((double)j)/4.0 * intensity;

    fprintf(lutt[0],"<-color{%f,%f,0.0}->\n",red,green);
    fprintf(mapt[0],"<-color{%f,%f,0.0}->\n",red,green);
  }

/* }}} */
  /* {{{ type t1 */

for (a=0; a<1; a+=0.01)
{
  fprintf(lutt[1],"<-color{%f,%f,%f}->\n",a,a,a);
  fprintf(mapt[1],"<-color{%f,%f,%f}->\n",a,a,a);
}

for(j=0; j<5; j++)
  for(k=0; k<K3; k++)
  {
    if (k<K1)              intensity = STARTINT;
    if ((k>=K1) && (k<K2)) intensity = ((double)k-K1)/((K2-K1-1)/(1-STARTINT)) + STARTINT;
    if (k>=K2)             intensity = 1;

    red=1.0 * intensity;
    green=((double)j)/4.0 * intensity;

    fprintf(lutt[1],"<-color{%f,%f,0.0}->\n",red,green);
    fprintf(mapt[1],"<-color{%f,%f,0.0}->\n",red,green);
  }

for(j=0; j<5; j++)
  for(k=0; k<K3; k++)
  {
    if (k<K1)              intensity = STARTINT;
    if ((k>=K1) && (k<K2)) intensity = ((double)k-K1)/((K2-K1-1)/(1-STARTINT)) + STARTINT;
    if (k>=K2)             intensity = 1;

    blue=1.0 * intensity;
    green=((double)j)/4.0 * intensity;

    fprintf(lutt[1],"<-color{0.0,%f,%f}->\n",green,blue);
    fprintf(mapt[1],"<-color{0.0,%f,%f}->\n",green,blue);
  }

for(j=0; j<5; j++)
  for(k=0; k<K3*2; k++)
  {
    if (k<K1*2)                intensity = STARTINT;
    if ((k>=K1*2) && (k<K2*2)) intensity = ((double)k-K1*2)/((K2*2-K1*2-1)/(1-STARTINT)) + STARTINT;
    if (k>=K2*2)               intensity = 1;

    green=(((double)j)/8.0+0.5) * intensity;

    fprintf(lutt[1],"<-color{0.0,%f,0.0}->\n",green);
    fprintf(mapt[1],"<-color{0.0,%f,0.0}->\n",green);
  }

/* }}} */

/* }}} */
  /* {{{ rest of material map */

for(l=0;l<NLUTS;l++)
{
  fprintf(map[l],"]\n/Names [\n");
  fprintf(mapt[l],"]\n/Names [\n");
}

for (j=0; j<i1; j++)
{
  fprintf(map[0],"(Matrl%d)\n",j);
  fprintf(mapt[0],"(Matrl%d)\n",j);
}
for (j=0; j<i2; j++)
{
  fprintf(map[1],"(Matrl%d)\n",j);
  fprintf(mapt[1],"(Matrl%d)\n",j);
} 
 
for(l=0;l<NLUTS;l++)
{
  fprintf(map[l],"]\n/MinValues [\n");
  fprintf(mapt[l],"]\n/MinValues [\n");
}

for (j=0; j<i1; j++)
{
  fprintf(map[0],"%f\n",((double)j)*32000.0/((double)i1));
  fprintf(mapt[0],"%f\n",((double)j)*32000.0/((double)i1));
}
for (j=0; j<i2; j++)
{
  fprintf(map[1],"%f\n",((double)j)*32000.0/((double)i2));
  fprintf(mapt[1],"%f\n",((double)j)*32000.0/((double)i2));
} 
 
for(l=0;l<NLUTS;l++)
{
  fprintf(map[l],"]\n/MaxValues [\n");
  fprintf(mapt[l],"]\n/MaxValues [\n");
}

for (j=1; j<=i1; j++)
{
  fprintf(map[0],"%f\n",((double)j)*32000.0/((double)i1));
  fprintf(mapt[0],"%f\n",((double)j)*32000.0/((double)i1));
}
for (j=1; j<=i2; j++)
{
  fprintf(map[1],"%f\n",((double)j)*32000.0/((double)i2));
  fprintf(mapt[1],"%f\n",((double)j)*32000.0/((double)i2));
}

for(l=0;l<NLUTS;l++)
{
  fprintf(map[l],"]\n/Opacities [\n");
  fprintf(mapt[l],"]\n/Opacities [\n");
}

for (j=0; j<i1/2; j++)
{
  fprintf(map[0],"%f\n",((double)j)*0.05/(0.5*(double)i1)+0.05);
  fprintf(mapt[0],"%f\n",((double)j)*0.05/(0.5*(double)i1)+0.05);
}
for (j=i1/2; j<i1; j++)
{
  fprintf(map[0],"0.1\n");
  fprintf(mapt[0],"0.1\n");
}

for (j=0; j<i2/5; j++)
{
  fprintf(map[1],"%f\n",((double)j)*0.05/(0.2*(double)i2)+0.05);
  fprintf(mapt[1],"%f\n",((double)j)*0.05/(0.2*(double)i2)+0.05);
}
for (j=i2/5; j<i2; j++)
{
  fprintf(map[1],"0.1\n");
  fprintf(mapt[1],"0.1\n");
}

for(l=0;l<NLUTS;l++)
{
  fprintf(map[l],"]\n/RenderMaterial? [\n");
  fprintf(mapt[l],"]\n/RenderMaterial? [\n");
}

for (j=1; j<=i1; j++)
{
  fprintf(map[0],"true\n");
  fprintf(mapt[0],"true\n");
}
for (j=1; j<=i2; j++)
{
  fprintf(map[1],"true\n");
  fprintf(mapt[1],"true\n");
}

/* }}} */
  /* {{{ end texts for luts and mmaps */

for(l=0;l<NLUTS;l++)
{
  fprintf(lut[l],"]\n>>\n\n%%%%EndInstance\n%%%%EOF\n");
  fprintf(map[l],"]\n>>\n\n%%%%EndInstance\n%%%%EOF\n");
  fprintf(lutt[l],"]\n>>\n\n%%%%EndInstance\n%%%%EOF\n");
  fprintf(mapt[l],"]\n>>\n\n%%%%EndInstance\n%%%%EOF\n");
}

/* }}} */

  return(0);
}

/* }}} */
