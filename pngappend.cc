/*  pngappend - simple programm to append sets of png

    Christian Beckmann and Matthew Webster, FMRIB Image Analysis Group

    2007 University of Oxford  */

/*  CCOPYRIGHT  */

#include <string>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include "gd.h"
#include "miscmaths/miscmaths.h"

using namespace std;

void usage(void)
{
  cerr << endl << "Usage: pngappend <input 1> <+|-> [n] <input 2> [<+|-> [n] <input n>]  output>" << endl;

  cerr << endl << " + appends horizontally," << endl << " - appends vertically (i.e. works like a linebreak)" << endl;
  cerr << "[n] number ofgap pixels" <<  endl;
  cerr << "note that files with .gif extension will be input/output in GIF format" << endl; 
  exit(1);
}

void help()
{
  cerr<< endl << " pngappend  -  append PNG files horizontally and/or vertically into a new PNG (or GIF) file" << endl;
  usage();
}
    
int max (int a, int b)
{
  if (a<b) return b;
  else return a;
}

int main(int argc, char *argv[])
{
  if ((argc<2)||(strcmp(argv[1],"-help")==0)||(strcmp(argv[1],"--help")==0)) help();

  if ((argc<3)) usage();
  int argidx = 1, black, toprx = 0, topry = 0, gap = 0;
  bool appendh = true;

  gdImagePtr im_out, im_read, im_tmp;
  FILE *in;
  string filename;
  bool GIFOUT;
  // read in the first image 

  if((in = fopen(argv[argidx++], "rb"))==NULL){
    cerr << endl<< " Cannot open " << argv[argidx-1] << " for reading" << endl; 
    fclose(in);
    exit(1);
  }
  else if((im_read = gdImageCreateFromPng(in))==NULL)
  {
    rewind(in);
    if ((im_read = gdImageCreateFromGif(in))==NULL)
    { 
      cerr << endl <<argv[argidx-1] << " is not a valid png2 or gif file" << endl;
      fclose(in);
      exit(1);
    }
  };
  fclose(in);

  //copy it to im_out 

  im_out = gdImageCreateTrueColor(im_read->sx,im_read->sy); //dImageCreate rather than TrueColour for GIF - does it matter?
  gdImageCopy(im_out, im_read, 0, 0, 0, 0,im_read->sx, im_read->sy);
  gdImageDestroy(im_read);

  toprx = im_out->sx;
  topry = 0;

  while(argidx < argc-1){
    
    //copy im_out to im_tmp
    im_tmp = gdImageCreateTrueColor(im_out->sx,im_out->sy);
    gdImageCopy(im_tmp, im_out, 0, 0, 0, 0,im_out->sx, im_out->sy);
    gdImageDestroy(im_out);

    //read token
    if(strcmp(argv[argidx],"+")==0)
      appendh = true;
    else if(strcmp(argv[argidx],"-")==0)
      appendh = false;
    else{
      cerr << endl << "ERROR: use '+' or '-' to indicate horizontal or vertical concatenation" 
	   << endl;
       usage();
    }
    argidx++;

    //read in new image
    if(MISCMATHS::isNumber(argv[argidx])){
      gap = atoi(argv[argidx]);
      argidx++;
    }

    if((in = fopen(argv[argidx], "rb"))==NULL){
	cerr << endl<< " Cannot open " << argv[argidx] << " for reading" << endl; 
	fclose(in);
	exit(1);
    }
    else if((im_read = gdImageCreateFromPng(in))==NULL)
    {
      rewind(in);
      if ((im_read = gdImageCreateFromGif(in))==NULL)
      { 
        cerr << endl <<argv[argidx-1] << " is not a valid png or gif file" << endl;
        fclose(in);
        exit(1);
      }
    };
    fclose(in);

    //copy everything into img_out
    {
      int newx, newy;
  
      if(!appendh){
	toprx = 0;
	topry = im_tmp->sy + gap;
      }else
	toprx += gap;      

      newx = max(toprx + im_read->sx, im_tmp->sx);
      newy = max(topry + im_read->sy, im_tmp->sy);

      im_out = gdImageCreateTrueColor(newx,newy);
      black = gdImageColorAllocate(im_out, 0, 0, 0); 
      gdImageCopy(im_out, im_tmp, 0, 0, 0, 0,im_tmp->sx, im_tmp->sy);
      gdImageCopy(im_out, im_read, toprx, topry, 0, 0, im_read->sx, im_read->sy);       

      toprx += im_read->sx;
    }

    gdImageDestroy(im_read);
    gdImageDestroy(im_tmp);

    argidx++;
  }
	
  //output im_out
  FILE *imageout;
  filename=string(argv[argc-1]);
  GIFOUT=(filename.substr(filename.size()-4,filename.size())==string(".gif"));
  if((imageout = fopen(argv[argc-1], "wb"))==NULL){
    cerr << endl <<"ERROR: cannot open " << argv[argc-1] << "for writing" << endl;
    exit(1);
  }
  if (GIFOUT)
  {
    im_tmp=gdImageCreate(im_out->sx,im_out->sy);
    gdImageCopy(im_tmp, im_out, 0, 0, 0, 0,im_out->sx, im_out->sy);
    gdImageGif(im_tmp, imageout);
    gdImageDestroy(im_tmp);
  }
  else gdImagePng(im_out, imageout);
  gdImageDestroy(im_out);
  fclose(imageout);

  return 0;
}
