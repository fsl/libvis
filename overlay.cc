
/*  overlay.c - combine two images for colour overlay

    Stephen Smith, Christian  Beckmann and Matthew Webster, FMRIB Image Analysis Group

    Copyright (C) 1999-2009 University of Oxford  */

/*  CCOPYRIGHT  */

#include "NewNifti/NewNifti.h"
#include "newimage/newimageall.h"

#include "miscpic.h"

using namespace std;
using namespace NiftiIO;
using namespace NEWIMAGE;
using namespace MISCPIC;

void usage(void)
{
  printf("Usage: overlay <colour_type> <output_type> [-c] <background_image> <bg_min> <bg_max> <stat_image_1> <s1_min> <s1_max> [stat_image_2 s2min s2max] <output_image> [cbartype] [cbarfilename]\n");
  printf("colour_type: 0=solid 1=transparent colours\n");
  printf("output_type: 0=floating point (32 bit real) 1=integer (16 bit signed integer)\n");
  printf("-c : use checkerboard mask for overlay\n");
  printf("<bg_min> <bg_max> can be replaced by -a for automatic estimation of background display range or -A to use the full image range\n");
  printf("valid cbartypes colours are: ybg, valid cbartypes options are: s (stack) \n");
  exit(1);
}

//template <class T>
int fmrib_main(int argc, char* argv[], bool out_int)
{
  int colour_type, argindex=1, checker=0;
  float bgmin, bgmax, s1min, s1max, s2min, s2max;
  bool debug = false;

  volume<float> bg, s1, s2;
  string cbarfname = "";
  string cbartype = "";

  colour_type=atoi(argv[argindex++]);
  argindex++;

  if (!strcmp(argv[argindex],"-c")) {
    checker=1;
    argindex++;
  }

  if (!strcmp(argv[argindex],"-d")) {
    debug=true;
    argindex++;
  }

  read_volume(bg,string(argv[argindex++]));

  if (!strcmp(argv[argindex],"-a")) {
    bgmax = bg.percentile(0.98);
    bgmin = bg.percentile(0.02);
    argindex++;
  } else if (!strcmp(argv[argindex],"-A")) {
    bgmax = bg.max();
    bgmin = bg.min();
    argindex++;
  } else {
    bgmin=atof(argv[argindex++]);
    bgmax=atof(argv[argindex++]);
  }

  read_volume(s1,string(argv[argindex++]));
  s1min=atof(argv[argindex++]);
  s1max=atof(argv[argindex++]);

  if (argc-argindex-1>2){
    read_volume(s2,string(argv[argindex++]));
    s2min=atof(argv[argindex++]);
    s2max=atof(argv[argindex++]);
  }
  else{
    s2 = s1; s2min= 0.0; s2max = 0.0;
  }

  if (argc-argindex-1==2){
    cbarfname = string(argv[argindex++]);
    cbartype = string(argv[argindex++]);
  }

  if(!argv[argindex]){
    cerr << "ERROR: Please specify an output filename " << endl << endl;
    exit(2);
  }
  else{
    //    miscpic<T> newpic;
    // volume<T> newvol;
    miscpic newpic;
    volume<float> newvol;

    newpic.overlay(newvol, bg, s1, s2, bgmin, bgmax, s1min, s1max,
		   s2min, s2max, colour_type, checker,
		   cbarfname, cbartype, out_int, debug);
    if(out_int)
      save_volume_dtype(newvol,string(argv[argindex]), DT_SIGNED_SHORT);
    else
      save_volume(newvol,string(argv[argindex]));

    return 0;
  }
}


int main(int argc,char *argv[])
{

  if (argc<9)
    usage();

  int otype;
  otype = atoi(argv[2]);

  // if (otype == 0)
//     return call_fmrib_main(DT_FLOAT,argc,argv);
//   else if (otype == 1)
//     return call_fmrib_main(DT_SIGNED_SHORT,argc,argv);
//   else
//     usage();

  if (otype == 0)
    return fmrib_main(argc,argv,false);
  else if (otype == 1)
    return fmrib_main(argc,argv,true);
  else
    usage();

}
