include ${FSLCONFDIR}/default.mk

PROJNAME = libvis
SOFILES  = libfsl-miscplot.so libfsl-miscpic.so
XFILES   = slicer overlay wpng pngappend fsl_tsplot fsl_boxplot fsl_histogram create_lut

LIBS = -lfsl-newimage -lfsl-miscmaths -lfsl-NewNifti -lfsl-znz \
       -lfsl-utils -lfsl-cprob -lgdc -lgd -lpng

all: libfsl-miscplot.so libfsl-miscpic.so ${XFILES}

libfsl-miscplot.so: miscplot.o
	$(CXX) $(CXXFLAGS) -shared -o $@ $^ ${LDFLAGS}

libfsl-miscpic.so: miscpic.o
	$(CXX) $(CXXFLAGS) -shared -o $@ $^ ${LDFLAGS}

%: %.o libfsl-miscplot.so libfsl-miscpic.so
	$(CXX) ${CXXFLAGS} -o $@ $< -lfsl-miscplot -lfsl-miscpic ${LDFLAGS}

wpng: writepng.o wpng.o
	$(CC) ${CFLAGS} -o $@ $^ -lfsl-miscplot -lfsl-miscpic ${LDFLAGS}

create_lut: create_lut.c
	$(CC) $(CFLAGS) -o create_lut create_lut.c
	@if [ ! -d ${DESTDIR}/etc ] ; then \
	  ${MKDIR} ${DESTDIR}/etc ; \
	  ${CHMOD} g+w ${DESTDIR}/etc ; \
	fi
	@if [ ! -d ${DESTDIR}/etc/luts ] ; then \
	  ${MKDIR} ${DESTDIR}/etc/luts ; \
	  ${CHMOD} g+w ${DESTDIR}/etc/luts ; \
	fi
	${CP} luts+pics/*.gif luts+pics/*.lut $(DESTDIR)/etc/luts
	./create_lut $(DESTDIR)/etc/luts/render
